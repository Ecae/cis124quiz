var rows, columns, width, height, row, square, bg;

$(document).ready(function () {

    $('#submitButton').click(function(e) {
        e.preventDefault();
        rows = $('#rows').val();
        columns = $('#columns').val();
        
        bg = "media/meme.png"
        
        width = rows * 100;
        height = columns * 100;
        
        $('.container').css('height', height+"px");
        $('.container').css('width', width+"px");
    
        $('#form').hide();
    });

    var monkeyDiv = $(".square");
    var monkeyPic;
    
    var message;
    
    var monkeyDiv2 = $(".square");
    var monkeyPic2;
    
    var i = 0;
    
    $('div').each(function () {
        $(this).click(function () {
            if(i === 0){
                monkeyPic = $(this).find("img").attr("src");
                message = $(this).find("img").attr("alt");
                monkeyDiv.css("background-image","url(" + monkeyPic + ")");
                i++;
            }
            else if(i === 1){
                monkeyPic2 = $(this).find("img").attr("src");
                message = $(this).find("img").attr("alt");
                monkeyDiv2.css("background-image","url(" + monkeyPic2 + ")");
                i++;
            }
            
            else{
                Matching(monkeyDiv, monkeyDiv2);
                monkeyDiv.css("background-image","url(../media/meme.png)");
                monkeyDiv2.css("background-image","url(../media/meme.png)");
                i = 0;

            }
        });
        
        function Matching(pic,pic2) {
            if(pic.css("background-image") === pic2.css("background-image")) {
                $("p").html(message);
            }
            else{
                $("p").html("That was unwise");
            }
        }
    });    
    
    $('#startGameButton').click(function() {
        row = $("<div />", {
            class: 'row'
            
        });
        square = $("<div />", {
            class: 'square'
            
        });
        $('#startGameButton').hide();
        initGame();
    });
    
    function initGame() {
        for (var i = 0; i < columns; i++) {
            row.append(square.clone());
        }
        for (var i = 0; i < rows; i++) {
            $(".container").append(row.clone());
        }
    }
    
});